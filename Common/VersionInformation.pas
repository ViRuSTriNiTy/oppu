unit VersionInformation;

interface

type
  TVersionInformation = class
    private
      fVersion: String;
      fFileDescription: String;
      fMajor: LongWord;
      fMinor: LongWord;
      fRelease: LongWord;
      fBuild: LongWord;

    public
      function Retrieve(const AFilename: String): Boolean;

      property Version: String read fVersion;
      property Major: LongWord read fMajor;
      property Minor: LongWord read fMinor;
      property Release: LongWord read fRelease;
      property Build: LongWord read fBuild;
      property FileDescription: String read fFileDescription;
  end;

implementation

uses
  Windows, SysUtils;

type
  PLanguageAndCodePageStruct = ^TLanguageAndCodePageStruct;
  TLanguageAndCodePageStruct = packed record
    wLanguage: Word;
    wCodePage: Word;
  end;

function TVersionInformation.Retrieve(const AFilename: String): Boolean;
var
  lVersionInfo: Pointer;
  lVersionInfoSize, lVersionValueSize, lDummy: LongWord;
  lVersionValue: PVSFixedFileInfo;
  lVersion: String;
  lLanguageAndCodePage: PLanguageAndCodePageStruct;
  lVersionString: PChar;
  lResult: Boolean;

begin
  result := false;

  fVersion := '';
  fMajor := 0;
  fMinor := 0;
  fRelease := 0;
  fBuild := 0;
  fFileDescription := '';

  lDummy := 0;
  lVersionInfoSize := GetFileVersionInfoSize(PChar(AFilename), lDummy);
  if lVersionInfoSize > 0 then begin

    GetMem(lVersionInfo, lVersionInfoSize);
    try

      if GetFileVersionInfo(PChar(AFilename), 0, lVersionInfoSize, lVersionInfo) then begin

        // get version
        lVersionValue := nil;
        lVersionValueSize := 0;

        lResult := VerQueryValue(lVersionInfo, '\', Pointer(lVersionValue), lVersionValueSize);

        if lResult then begin
          fMajor := lVersionValue^.dwFileVersionMS shr 16;
          fMinor := lVersionValue^.dwFileVersionMS and $FFFF;

          lVersion := IntToStr(fMajor) + '.' + IntToStr(fMinor);

          if (lVersionValue^.dwFileVersionLS shr 16 > 0) or
             (lVersionValue^.dwFileVersionLS and $FFFF > 0)
          then begin
            fRelease := lVersionValue^.dwFileVersionLS shr 16;

            lVersion := lVersion + '.' + IntToStr(fRelease);
          end;

          if lVersionValue^.dwFileVersionLS and $FFFF > 0 then begin
            fBuild := lVersionValue^.dwFileVersionLS and $FFFF;

            lVersion := lVersion + '.' + IntToStr(fBuild);
          end;

          fVersion := lVersion;
        end;

        // get language and code page list
        lLanguageAndCodePage := nil;

        // note: lVersionValueSize might be a multiple of SizeOf(lLanguageAndCodePage^)
        // when more than one language is stored in version info
        lResult := VerQueryValue(lVersionInfo,
                                 PChar('\VarFileInfo\Translation'),
                                 Pointer(lLanguageAndCodePage),
                                 lVersionValueSize);

        if lResult then begin
          if lVersionValueSize >= SizeOf(lLanguageAndCodePage^) then begin

            // retrieve file description for first language and code page
            lVersionString := nil;

            lResult := VerQueryValue(lVersionInfo,
                                     PChar('\StringFileInfo\' + IntToHex(lLanguageAndCodePage^.wLanguage, 4) + IntToHex(lLanguageAndCodePage^.wCodePage, 4) + '\FileDescription'),
                                     Pointer(lVersionString),
                                     lVersionValueSize);

            if lResult then
              fFileDescription := lVersionString;
          end;
        end;

        result := true;

      end;

    finally
      FreeMem(lVersionInfo, lVersionInfoSize);
    end;
  end;
end;

end.
